@extends('layout.app')

@section('title', 'create')

@section('content')

    <form action="{{ route('projects.store') }}" method="post">
        @csrf    
        <input type="text" name="title" value="">
        <input type="text" name="description" value="">
        <input type="date" name="date" value="">
        <input type="submit" value="Create">
    </form>

@endsection