@extends('layout.app')

@section('title', 'update')

@section('content')
    <form action="{{ route('projects.update', ['project' => $project->id]) }}" method="post">
        @csrf
        <input type="text" name="title" value="{{ $project->title }}">
        <input type="text" name="description" value="{{ $project->description }}">
        <input type="date" name="date" value="{{ $project->date }}">
        @method('PUT')
        <input type="submit" value="Update">
    </form>

@endsection