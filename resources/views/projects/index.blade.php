@extends('layout.app')

@section('title', 'random')

@section('content')
    <ul>
        @forelse($projects as $project)

            <li>
                {{ $project->title }}

                <form action="{{ route('projects.destroy', ['project' => $project->id]) }}" method="post">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="delete">
                </form>
            </li>

        @empty
            <p>No projects</p>
        @endforelse
    </ul>
@endsection