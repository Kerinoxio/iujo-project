<?php

namespace App\Http\Controllers;

use App\Models\Projects;
use GuzzleHttp\Handler\Proxy;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use SebastianBergmann\CodeCoverage\Report\Xml\Project;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Projects::get();
        return View::make('projects.index', ['projects' => $projects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $projects = new Projects;
        $projects->title = $request->input('title');
        $projects->description = $request->input('description');
        $projects->date = $request->input('date');
        $projects->save();    
        return redirect()->route('projects.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = Projects::where('id', $id)->first();
        return View::make('projects.show', ['project' => $title]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = Projects::where('id', $id)->first();
        return View::make('projects.edit', ['project' => $title]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $title = Projects::find($id);
        $title->title = $request->input('title');
        $title->description = $request->input('description');
        $title->date = $request->input('date');
        $title->save();
        return redirect()->route('projects.show', ['project' => $title]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $projects = Projects::where('id', $id)->delete();
        return redirect()->route('projects.index');
    }
}
